#!/bin/bash

set -e 
set -x

docker-compose pull
docker-compose build --pull --no-cache

docker-compose up -d
